const balanceElement = document.getElementById("balance");
const payElement = document.getElementById("pay");
const workElement = document.getElementById("workBtn");
const bankElement = document.getElementById("bankBtn");
const loanElement = document.getElementById("loanBtn");
const loanLabelElement = document.getElementById("loanLbl");
const outAmountElement = document.getElementById("outAmount");
const repayElement = document.getElementById("repayBtn");
const laptopsElement = document.getElementById("laptopList");
const featureDescElement = document.getElementById("featureDesc");
const laptopNameElement = document.getElementById("laptopName");
const priceElement = document.getElementById("price");
const laptopDescElement = document.getElementById("description");
const buyBtnElement = document.getElementById("buyBtn");
const imgElement = document.getElementById("laptopImg");

let currentTitle = "";
let currentPrice = 0;
let haveDebt = false;
let haveLoan = false;
let laptops = [];
balanceElement.textContent = 0;
payElement.textContent = 0;

//Fetches the computer information to populate the computer list
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptops(laptops))
    .catch(function (error) {
        console.error("rip", error);
    });

workElement.addEventListener("click", work);
bankElement.addEventListener("click", bankPay);
loanElement.addEventListener("click", getALoan);
repayElement.addEventListener("click", repayLoan);
laptopsElement.addEventListener("change", handleLaptopChange);
buyBtnElement.addEventListener("click", buyNow);


function getALoan() {
    //starts a prompt expecting a non zero positive number. The loan cannot exceed two times the balance value.
    //If the loan goes through makes the outstanding loan and repay loan elements visible.
    //Sets Loan and debt flags to true to block additional loans until certain conditions are met.
    if (!haveLoan) {
        const loanValue = parseInt(prompt("Amount to loan:"));
        if (isNaN(loanValue) || loanValue <= 0) {
            alert("Not a valid number.");
        }
        else {
            if (parseInt(loanValue) > parseInt(balanceElement.textContent) * 2) {
                alert("Cannot loan an amount greater than two times current balance value.")
            } else {
                if (loanLabelElement.style.display != "none") {
                    loanLabelElement.textContent = "Outstanding loan:";
                    outAmountElement.textContent = loanValue;
                }
                else {
                    loanLabelElement.style.display = "inline";
                    outAmountElement.style.display = "inline";
                    outAmountElement.textContent = loanValue;
                }
                balanceElement.textContent = parseInt(balanceElement.textContent) + parseInt(loanValue);
                repayElement.style.display = "inline";
                haveLoan = true;
                haveDebt = true;
            }
        }

    }
    else {
        alert("Cannot apply for a loan at this time.");
    }




}

function bankPay() {
    //transfer pay to bank balance. If there is an outstanding loan 10% of pay will go to the loan.
    //If the loan gets fully payed back hide appropriate elements and transfer any surplus money to the balance element.
    if (!haveLoan) {
        balanceElement.textContent = parseInt(balanceElement.textContent) + parseInt(payElement.textContent);
    }
    else {
        balanceElement.textContent = parseInt(balanceElement.textContent) + parseInt(payElement.textContent) * 0.9;
        if (parseInt(payElement.textContent) * 0.1 >= parseInt(outAmountElement.textContent)) {
            balanceElement.textContent = parseInt(balanceElement.textContent) + parseInt(payElement.textContent) * 0.1 - parseInt(outAmountElement.textContent);
            loanLabelElement.style.display = "none";
            outAmountElement.style.display = "none";
            repayElement.style.display = "none";
            haveDebt = false;
        }
        outAmountElement.textContent = parseInt(outAmountElement.textContent) - parseInt(payElement.textContent) * 0.1;
    }
    payElement.textContent = 0;
}

function work() {
    // increases the pay balance by 100
    payElement.textContent = parseInt(payElement.textContent) + 100;
}

function repayLoan() {
    //Reduces the outstanding balance by the amount in pay. and sets pay to 0.
    //If the loan gets fully payed back hide appropriate elements and transfer any surplus money to the balance element.
    //Resets the haveDebt flag.
    if (parseInt(payElement.textContent) >= parseInt(outAmountElement.textContent)) {
        balanceElement.textContent = parseInt(balanceElement.textContent) + parseInt(payElement.textContent) - parseInt(outAmountElement.textContent);
        loanLabelElement.style.display = "none";
        outAmountElement.style.display = "none";
        repayElement.style.display = "none";
        haveDebt = false;
    } else {
        outAmountElement.textContent = parseInt(outAmountElement.textContent) - parseInt(payElement.textContent);
    }
    payElement.textContent = 0;
}

function addLaptops(laptops) {
    //Calls te addLaptop() function for every element in the laptops array.
    //Displays correct computer information on the page.
    laptops.forEach(laptop => addlaptop(laptop));
    featureDescElement.innerText = laptops[0].specs;
    laptopNameElement.innerText = laptops[0].title;
    priceElement.innerText = `${laptops[0].price} Kr`;
    laptopDescElement.innerText = laptops[0].description;
    imgElement.src = laptops[0].image;
    currentPrice = parseInt(laptops[0].price);
    currentTitle = laptops[0].title;
}

function addlaptop(laptop) {
    //Adds the given laptop to the selection list.
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsElement.appendChild(laptopElement);
}

function handleLaptopChange(laptop) {
    //Changes the displayed information on the page based on chosen laptop in the selection element.
    const selectedLaptop = laptops[laptop.target.selectedIndex];
    featureDescElement.innerText = selectedLaptop.specs;
    laptopNameElement.innerText = selectedLaptop.title;
    priceElement.innerText = `${selectedLaptop.price} Kr`;
    laptopDescElement.innerText = selectedLaptop.description;
    imgElement.src = selectedLaptop.image;
    currentPrice = parseInt(selectedLaptop.price);
    currentTitle = selectedLaptop.title;
}

function buyNow() {
    //Displays an alert, resets haveLoan flag and reduces balance by price on successful purchase.
    //Displays an appropriate alert if the user cannot afford the selected computer.
    if (parseInt(balanceElement.textContent) < currentPrice) {
        alert("You cannot afford this product.")
    }
    else {
        balanceElement.innerText = parseInt(balanceElement.innerText) - currentPrice;
        alert(`Congratulation you are now an owner of ${currentTitle}.`)
        haveLoan = false;
    }
}