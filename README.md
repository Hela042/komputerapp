# KomputerApp

This is my implementation of the KomputerApp assignment.
I downloaded the images and put them in a folder locally because the fifth image in the API has the wrong extension.

## Installation

No need to install anything.

## Usage

Open the html file using live server in vs code or similar method. Alternatively use this link to access the app https://hela042.gitlab.io/komputerapp/.

## Author

Lukas Mårtensson @Hela042
